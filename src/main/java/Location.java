/**
 * Author: William Ong
 * Unit: Software Engineering Concepts - Assignment 1
 * ID: 19287368
 */

public class Location
{
    private final int x;
    private final int y;

    public Location(int x, int y)
    {
        this.x = x;
        this.y = y;
    }

    public int getX()
    {
        return x;
    }

    public int getY()
    {
        return y;
    }
}


