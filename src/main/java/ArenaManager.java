/**
 * Author: William Ong
 * Unit: Software Engineering Concepts - Assignment 1
 * ID: 19287368
 */
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public class ArenaManager
{
    // Queue of Locations, contains all of the location that is occupied by the robots
    private final BlockingQueue<Location> arena;

    /**
     * Constructor of ArenaManager
     * @param width width of the arena
     * @param height height of the arena
     */
    public ArenaManager(int width, int height)
    {
        arena = new ArrayBlockingQueue<>(width * height); // Convert 2D structure to a queue
    }

    /**
     * Add location to be occupied into the queue
     * @param l the location to be occupied
     * @return A boolean to state if the operation succeed or not, true means it has successfully occupy the
     *         location, otherwise, it hasn't
     */
    public boolean queueLocation(Location l)
    {
        boolean empty = false;
        try
        {
            // In-built contains(Object obj) does not work as intended
            // Check if there is an occupation existing
            if(!checkOccupation(l))
            {
                arena.put(l);
                empty = true;
            }
        }
        catch(InterruptedException e)
        {
            e.printStackTrace();
        }
        return empty;
    }

    /**
     * Free a location that exist on the queue
     * @param l the location to be freed
     */
    public void freeLocation(Location l)
    {
        arena.remove(l);
    }

    /**
     * Check if the location has been occupied or not
     * @param l the location to be checked
     * @return a boolean that state it exist in the queue or it has not
     */
    private boolean checkOccupation(Location l)
    {
        boolean locationStatus = false;

        for(Location existing_location : arena)
        {
            if (l.getX() == existing_location.getX() && l.getY() == existing_location.getY())
            {
                locationStatus = true; // There occupies a space, break out of it
                break;
            }
        }

        return locationStatus;
    }
}
