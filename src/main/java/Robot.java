/**
 * Author: William Ong
 * Unit: Software Engineering Concepts - Assignment 1
 * ID: 19287368
 */
import javax.swing.*;
import java.net.URL;
import java.util.Random;

public class Robot
{
    // Constant values
    private final Random r = new Random();

    // Arbitrary values
    private final int id;
    private double x;
    private double y;
    private final int delay;
    private final ImageIcon icon;
    private Thread thread;

    // Location values
    private Location previousLocation;
    private Location destination;

    // Arena manager
    private final ArenaManager arenaManager;

    /**
     * The Robot class primary purpose is to wander around until it gets shot by the player or enters the castle
     * @param id the unique identification of each robot
     * @param x the x coordinate of the robot
     * @param y the y coordinate of the robot
     * @param delay the time for the robot to wait before it makes its move
     * @param previousLocation the previous location set by the spawner
     * @param arenaManager the reference to the entire map
     */
    public Robot(int id, double x, double y, int delay, Location previousLocation, ArenaManager arenaManager)
    {
        this.id = id;
        this.x = x;
        this.y = y;
        this.delay = delay;
        this.previousLocation = previousLocation;
        this.arenaManager = arenaManager;

        // Initialize image
        URL robot_url = getClass().getClassLoader().getResource(SwingArena.ROBOT_FILE);
        if(robot_url == null)
        {
            throw new AssertionError("Cannot find image file " + SwingArena.ROBOT_FILE);
        }
        icon = new ImageIcon(robot_url);
    }

    /**
     * Initializes the task to start moving the robot and creates a thread for that purpose.
     */
    public void start()
    {
        Runnable task = () ->
        {
            while(!SwingArena.game_over)
            {
                try
                {
                    Thread.sleep(delay); // Wait before moving

                    // Decide destination
                    decideDestinationInAdvance();
                    int move_attempt = 0;
                    // While the destination is within map boundary or destination is currently occupied
                    while(!boundaryCheck(destination) || !arenaManager.queueLocation(destination))
                    {
                        // Decide destination again
                        decideDestinationInAdvance();

                        // A robot gets total of 4 move attempts until it is considered "boxed-in"
                        move_attempt++;
                        if(move_attempt == 4 && thread != null)
                        {
                            System.out.println(thread.getName() + ": Currently boxed in. Waiting for another " + delay + " milliseconds.");
                            Thread.sleep(delay);
                            move_attempt = 0;
                        }
                    }
                    // After this line means that the destination has been successfully chosen

                    // While the destination has not been reached, move 0.1 towards the destination every 50 milliseconds
                    while(Math.abs(x - destination.getX()) > 0.1 ||
                          Math.abs(y - destination.getY()) > 0.1)
                    {
                        move(destination);
                        Thread.sleep(50);
                    }

                    // Fix the position in as an extra pre-caution
                    x = destination.getX();
                    y = destination.getY();

                    // Free the previous location so other robot can occupy it
                    arenaManager.freeLocation(previousLocation);

                    // Set the previous location to the former destination as it has fully moved to its destination
                    previousLocation = destination;

                    // Game over condition check
                    // Fixed: Thread was set to null but the game over condition was set by a null thread,
                    //        changed to only valid threads are able to set the game over condition.
                    if(reachedCastle() && thread != null)
                    {
                        SwingArena.game_over = true;
                    }
                }
                catch(InterruptedException e)
                {
                    System.out.println("Robot " + id  + " is kil.");
                }
            }

            // Loop ends, game over
            if(thread != null)
                stop();
        };
        thread = new Thread(task, "robot-" + id + "-thread");
        thread.start();
    }

    /**
     * Kills the robot thread
     */
    public void stop()
    {
        System.out.println("Shutting down " + thread.getName() + "...");
        thread.interrupt();
        thread = null;
    }

    /**
     * Moves the robot according to its destination, increment by 0.1 for every 50 millisecond
     * @param destination the destination the robot is heading towards
     */
    private void move(Location destination)
    {
        if(x - destination.getX() > 0) // Left
            x -= 0.1;

        if(x - destination.getX() < 0) // Right
            x += 0.1;

        if(y - destination.getY() > 0) // Up
            y -= 0.1;

        if(y - destination.getY() < 0) // Down
            y += 0.1;
    }

    /**
     * Decides the robot's next destination in advance to occupy that space so other robots can't
     * enter the occupied space
     */
    private void decideDestinationInAdvance()
    {
        int direction = r.nextInt(4);
        int destX = (int)x;
        int destY = (int)y;
        switch(direction)
        {
            case 0:
                destX++;
                break;
            case 1:
                destX--;
                break;
            case 2:
                destY++;
                break;
            case 3:
                destY--;
                break;
        }
        destination = new Location(destX, destY);
    }

    /**
     * Constantly check if the location is within the boundary
     * @param location the location to be checked
     * @return a boolean of whether the location is within boundary or not
     */
    private boolean boundaryCheck(Location location)
    {
        boolean check = true;
        if(location.getX() > SwingArena.gridWidth - 1 || location.getX() < 0 ||
           location.getY() > SwingArena.gridHeight - 1 || location.getY() < 0)
        {
            check = false;
        }
        return check;
    }

    public double getX()
    {
        return x;
    }

    public double getY()
    {
        return y;
    }

    public int getId()
    {
        return id;
    }

    public int getDelay()
    {
        return delay;
    }

    public ImageIcon getIcon()
    {
        return icon;
    }

    public Location getPreviousLocation()
    {
        return previousLocation;
    }

    public Location getDestination()
    {
        return destination;
    }

    /**
     * Check if the robot has reached the castle, return if true
     * @return the boolean check
     */
    private boolean reachedCastle()
    {
        return Math.round(x) == SwingArena.castle_x && Math.round(y) == SwingArena.castle_y;
    }
}
