/**
 * Author: William Ong
 * Unit: Software Engineering Concepts - Assignment 1
 * ID: 19287368
 */

import java.awt.*;
import javax.swing.*;

public class App 
{
    public static void main(String[] args) 
    {
        // Note: SwingUtilities.invokeLater() is equivalent to JavaFX's Platform.runLater().
        SwingUtilities.invokeLater(() ->
        {
            // Logger list, contains all of the events that may occur during the game and display it when it happen
            DefaultListModel<String> logs = new DefaultListModel<>();
            JToolBar toolbar = new JToolBar();
            // Display the score to the user
            JLabel label = new JLabel("Score: 0");

            // Window title
            JFrame window = new JFrame("Vtuber Invasion");
            // Arena construction
            SwingArena arena = new SwingArena(logs, label);
            arena.addListener((x, y) ->
            {
                // Debug messages
                System.out.println("Debugging: Arena click at (" + x + "," + y + ")");
            });

            // Attach score label to the GUI
            toolbar.add(label);

            // Create a scroll pane dedicated for the game logs
            JScrollPane loggerArea = new JScrollPane(new JList<>(logs));
            loggerArea.setBorder(BorderFactory.createEtchedBorder());
            
            JSplitPane splitPane = new JSplitPane(
                JSplitPane.HORIZONTAL_SPLIT, arena, loggerArea);

            Container contentPane = window.getContentPane();
            contentPane.setLayout(new BorderLayout());
            contentPane.add(toolbar, BorderLayout.NORTH);
            contentPane.add(splitPane, BorderLayout.CENTER);
            
            window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            window.setPreferredSize(new Dimension(800, 800));
            window.pack();
            window.setVisible(true);
            
            splitPane.setDividerLocation(0.75);
        });
    }    
}
