/**
 * Author: William Ong
 * Unit: Software Engineering Concepts - Assignment 1
 * ID: 19287368
 */

/**
 * Represents an event handler for when the arena is clicked.
 */
public interface ArenaListener
{
    void squareClicked(int x, int y);
}
