/**
 * Author: William Ong
 * Unit: Software Engineering Concepts - Assignment 1
 * ID: 19287368
 */
import javax.swing.*;
import java.text.DecimalFormat;
import java.util.Random;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class RobotManager
{
    private static final int SPAWN_DELAY = 2000;
    private static final int MAX_DELAY = 2000;
    private static final int MIN_DELAY = 500;
    private static final int FIRE_DELAY = 1000;
    private static final int SCORING_DELAY = 1000;
    private static final int MAX_NO_OF_ROBOTS = SwingArena.gridHeight * SwingArena.gridWidth;

    private final Random r = new Random();
    private int id;
    private int score;
    private long start_time, bonus_delay;
    private Thread manager_thread;
    private Thread kill_thread;
    private Thread score_thread;
    private final ExecutorService es;
    private final BlockingQueue<Robot> robots = new ArrayBlockingQueue<>(MAX_NO_OF_ROBOTS);
    private final BlockingQueue<Location> firing_commands = new ArrayBlockingQueue<>(MAX_NO_OF_ROBOTS);
    private final DefaultListModel<String> logs;
    private final ArenaManager arenaManager;
    private final JLabel label;
    private final DecimalFormat df2 = new DecimalFormat("#.##");

    /**
     * The robot manager
     * @param logs to record the logs
     * @param arenaManager the reference to the entire grid
     * @param label the score label
     */
    public RobotManager(DefaultListModel<String> logs, ArenaManager arenaManager, JLabel label)
    {
        // Log messages
        this.logs = logs;
        // Score label
        this.label = label;
        // Unique id initialization
        id = 1;
        // Score initialization
        score = 0;
        // Bonus delay initialization
        start_time = 0;
        bonus_delay = 0;
        // Thread pool initialization
        es = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());
        // Arena manager dependency injection
        this.arenaManager = arenaManager;
    }

    /**
     * Starts three threads:
     *  - To spawn the robots
     *  - To kill the robots
     *  - To update the score label
     */
    public void start()
    {
        Runnable manager_task = () ->
        {
            while(!SwingArena.game_over)
            {
                try
                {
                    // Spawn delay timer, to prevent resource hoarding and establish thread timing
                    Thread.sleep(SPAWN_DELAY);

                    int spawnX = 0, spawnY = 0;
                    int choice = r.nextInt(4);
                    switch(choice)
                    {
                        case 0:
                            spawnX = 0;
                            spawnY = 0;
                            break;
                        case 1:
                            spawnX = SwingArena.gridWidth - 1;
                            spawnY = 0;
                            break;
                        case 2:
                            spawnX = 0;
                            spawnY = SwingArena.gridHeight - 1;
                            break;
                        case 3:
                            spawnX = SwingArena.gridWidth - 1;
                            spawnY = SwingArena.gridHeight - 1;
                            break;
                    }

                    // If the robot size is still lesser than the maximum amount of robot, keep queueing
                    if(robots.size() < MAX_NO_OF_ROBOTS)
                    {
                        Location spawnLocation = new Location(spawnX, spawnY);
                        // If the chosen corner location is available, add robot.
                        if(arenaManager.queueLocation(spawnLocation))
                        {
                            logs.addElement("Spawned at " + String.format("(%d, %d)", spawnX, spawnY));
                            addRobot(new Robot(generateID(), spawnLocation.getX(), spawnLocation.getY(), r.nextInt(MAX_DELAY - MIN_DELAY) + MIN_DELAY, spawnLocation, arenaManager));
                        }
                        else
                        {
                            System.out.println("Spawn space occupied at coordinate: " + String.format("(%d, %d)!", spawnX, spawnY));
                        }
                    }
                    else
                    {
                        System.out.println("Maximum number of robot reached!");
                    }
                }
                catch(InterruptedException e)
                {
                    System.out.println("Shutting down robot manager...");
                }
            }

            // Loop ends, game over
            if(manager_thread != null)
                stop();
        };
        manager_thread = new Thread(manager_task, "manager-thread");
        manager_thread.start();

        Runnable kill_task = () ->
        {
            while(!SwingArena.game_over)
            {
                try
                {
                    if(!firing_commands.isEmpty())
                    {
                        // Time elapsed from player clicking on the square and the firing command being executed
                        bonus_delay = System.currentTimeMillis() - start_time;

                        // Concurrent operation, shoot while checking if its been hit or not
                        if(!shoot(firing_commands.take(), bonus_delay))
                        {
                            logs.addElement("Missed!");
                        }
                        Thread.sleep(FIRE_DELAY);
                    }
                }
                catch(InterruptedException e)
                {
                    System.out.println("Shutting down firing commands...");
                }
            }
        };
        kill_thread = new Thread(kill_task, "kill-thread");
        kill_thread.start();

        Runnable score_task = () ->
        {
            while(!SwingArena.game_over)
            {
                try
                {
                    score += 10;
                    label.setText("Score: " + score);
                    Thread.sleep(SCORING_DELAY);
                }
                catch(InterruptedException e)
                {
                    System.out.println("Shutting down scoring system...");
                }
            }
        };
        score_thread = new Thread(score_task, "score-thread");
        score_thread.start();
    }

    /**
     * Add a robot to the queue for the GUI to repaint whilst being sent to the thread pool executor
     * @param r the robot to be added
     */
    public void addRobot(Robot r)
    {
        try
        {
            robots.add(r); // Add to the queue for repainting
            es.submit(r::start); // Submit task to executor to begin executing the task until it dies
        }
        catch (IllegalStateException e)
        {
            // Occurs when queue is full
            System.out.println(e.getMessage());
        }
    }

    /**
     * Generate a unique ID for every robot created
     * @return the id
     */
    public int generateID()
    {
        return id++;
    }

    public BlockingQueue<Robot> getQueue()
    {
        return robots;
    }

    /**
     * Queue up the firing commands to be executed in a timely manner
     * @param kill_zone the location to be hit
     * @param start_time the time since the player has clicked on the grid
     */
    public void queueFiringCommands(Location kill_zone, long start_time)
    {
        try
        {
            this.start_time = start_time;
            firing_commands.add(kill_zone);
        }
        catch(IllegalStateException e)
        {
            System.out.println(kill_thread.getName() + ": " + e.getMessage());
        }
    }

    /**
     * Commence the operation to remove the robot off the grid entirely
     * @param fired_location the location to be hit
     * @param bonus_delay the calculated bonus delay
     * @return a boolean if the robot has been hit or not
     */
    public boolean shoot(Location fired_location, long bonus_delay)
    {
        boolean hit = false;

        for(Robot robot : robots)
        {
            int firedX = fired_location.getX();
            int firedY = fired_location.getY();
            if(matchTargetedArea(robot.getPreviousLocation(), robot.getDestination(), firedX, firedY))
            {
                // Log robot destruction
                logs.addElement("Destroyed robot at " + String.format("(%s, %s)!", df2.format(robot.getX()), df2.format(robot.getY())));

                // Bonus calculation
                double bonus = bonus_delay/(float)robot.getDelay();
                score += 10 + 100 * bonus;

                // Deallocate the robot
                freeRobot(robot, robot.getPreviousLocation(), robot.getDestination());
                hit = true;
                break;
            }
        }
        return hit;
    }

    /**
     * Match the targeted area to see if any robot has been hit
     * @param previous the location where the robot was
     * @param destination the destination of the robot
     * @param firedX the x fired coordinate
     * @param firedY the y fired coordinate
     * @return a boolean if there exist a robot in that location
     */
    private boolean matchTargetedArea(Location previous, Location destination, int firedX, int firedY)
    {
        boolean check = false;

        // If its still within the previous space
        if(Math.abs(previous.getX() - firedX) < 0.1 && Math.abs(previous.getY() - firedY) < 0.1)
            check = true;

        // Note: This will throw a NullPointerException because there is a possibility that the destination has not been chosen
        if(destination != null)
            // If its still moving towards the destination
            if(Math.abs(destination.getX() - firedX) < 0.1 && Math.abs(destination.getY() - firedY) < 0.1)
                check = true;

        return check;
    }

    /**
     * Free the robot if it has been hit
     * @param robot the robot to be removed
     * @param previous the previous location of the robot
     * @param destination the destination of the robot (if there is any)
     */
    private void freeRobot(Robot robot, Location previous, Location destination)
    {
        // Kill the robot thread
        robot.stop();

        // Remove robot from repainting
        robots.remove(robot);

        // Free the robot's previous occupation space (always spawns with a previous location)
        arenaManager.freeLocation(previous);

        // Free the robot's destination space (if any exist at the moment)
        if(destination != null)
            arenaManager.freeLocation(destination);
    }

    /**
     * Stop all the threads and the thread pool
     */
    private void stop()
    {
        System.out.println("Shutting down " + manager_thread.getName() + "...");
        manager_thread.interrupt();
        manager_thread = null;

        System.out.println("Shutting down " + kill_thread.getName() + "...");
        kill_thread.interrupt();
        kill_thread = null;

        System.out.println("Shutting down " + score_thread.getName() + "...");
        score_thread.interrupt();
        score_thread = null;

        System.out.println("Shutting down thread pool...");
        es.shutdown();
    }
}
